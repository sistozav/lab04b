///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04b - Hello World II
///
/// This program has both Object Oriented and procedural elements in it
///
/// @file both_worlds.cpp
/// @version 1.0
///
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>     // The procedural stdio.h
#include <iostream>    // The object-oriented version of stdio.h

using namespace std;   // If we didn't have this, then we'd have to put std::
                       // in front of everything we used from the std include
                       // files.

int main() {
	printf( "Hello from printf()\n" );  // This still works

   cout << "Hello World\n";        // And this works...

   cout << "Hello" << " " << "World\n";  // The strings are concatinated together

   cout << "Hello" << " " << "World" << endl;  // Replace \n with endl (an object that outputs '\n')

   std::cout << "Hello World" << std::endl;  // If we didn't have "using namespace std", 
                                             // Then everything we'd get from the C standard
                                             // would have to start with std::

   std::string hello = std::string("Hello World\n");  // I've just allocated an object... its
                                                      // datatype is a string.

   std::cout << hello;

   cout << "I've said Hello " << 6 << " times\n";  // I can easily print numbers without printf("%d")

   return 0;
}

